# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 09:45:33 2021

@author: holmanf
"""
from osgeo import ogr, gdal, osr
import pandas as pd
import xlsxwriter
import os, re
import glob
import rasterio
from rasterio.mask import mask
from rasterio.warp import calculate_default_transform
from rasterio.windows import from_bounds
import numpy as np
from shapely.geometry import shape
from shapely.geometry.multipolygon import MultiPolygon
from rasterio.features import rasterize
import geopandas

# def get_raster_projection(inRaster):
#     '''
#     Return crs EPSG code of input raster.

#     Parameters
#     ----------
#     inRaster : rasterio Dataset
#         rasterio Dataset from which crs will be determined.

#     Returns
#     -------
#     spatial_ref : str
#         string of crs projection in format: 'epsg:xxxx'.

#     '''
#     spatial_ref = rasterio.open(inRaster).crs.data['init']
#     return spatial_ref

def overlaps_flightline(flightline, shapefiles):
    '''
    Select only shapefile which overlap flightline.
    
    Transform GeoJSON crs to match flightline CRS.

    Parameters
    ----------
    flightline : rasterio Dataset
        Any geospatial raster dataset, which overlapping shapefiles will be tested.
    shapefiles : GeoJSON
        GeoJSON file of AOI polygons, CRS:4326.

    Returns
    -------
    a : geopandas DataFrame
        Dataframe of polygons that overlap flightline.

    '''

    with rasterio.open(flightline) as img_rstr:
        bnd_rstr = img_rstr.read(int(img_rstr.count/2))
        rstr_crs = img_rstr.crs.data['init']
        
    bnd_rstr[bnd_rstr>0]=1
    shapes = list(rasterio.features.shapes(bnd_rstr, transform=img_rstr.transform))    
          
    polygons = [shape(geom) for geom, value in shapes if value == 1]
    multipolygon = MultiPolygon(polygons)
    
    pandaPlots = geopandas.read_file(shapefiles)
    pandaPlots = pandaPlots.to_crs(rstr_crs)
    identifier = [b for b in pandaPlots.columns if str(pandaPlots[b].dtype) == 'int64' and pandaPlots[b].max() == len(pandaPlots)][0]
    a = [b for b in pandaPlots[identifier] if pandaPlots[pandaPlots[identifier]==b].geometry.iloc[0].within(multipolygon)]
    
    df = pandaPlots[pandaPlots[identifier].isin(a)]
    return df

def rasterMasker(inPolygon, inRaster):
    '''
    Step 1) Window (subset) and rasterize GeoJSON polygon to same extent as inRaster.
    
    Step 2) Return numpy masked array of same dimensions as inRaster.

    Parameters
    ----------
    inPolygon : GeoJSON Polygon
        CRS needs to match inRaster.
    inRaster : rasterio Dataset
        Dataset to be masked.
    
    Returns
    -------
    Maked Numpy Array.

    '''
    with rasterio.open(inRaster) as src:
        bands = src.count
        shp = inPolygon
        window = src.window(*shp.iloc[0].bounds)
        rows,cols = src.read(int(src.count/2), window=from_bounds(*shp.iloc[0].bounds, transform=src.transform),boundless=True).shape
        res = rasterize(shapes=([(shp.geometry,shp.name)]),out_shape=(rows,cols),transform=src.window_transform(window))
        res = np.repeat(res[np.newaxis,...],(bands),0)
        res = np.ma.masked_array(res, res!=shp.name)
        array = src.read(window=from_bounds(*shp.iloc[0].bounds, transform=src.transform))
        result = np.ma.masked_where(res!=shp.name,array)
        return result

def extract_hyperspec(datafile, shapefiles):
    '''
    1) Isolate polygons which cover the same area as datafile.
    
    2)Sample means of individual datafile layers/bands from each overlapping polygon.
    
    Utilises rasterio window read to minimise memory usage.

    Parameters
    ----------
    datafile : rasterio Datafile
        Any datafile that can be read by rasterio.
    shapefiles : GeoJSON
        GeoJSON file of polygonised AOIs, CRS = 4326.
    polygonIds : list
        List of uniqe ID values to filter shapefiles by.

    Returns
    -------
    df : pandas DataFrame
        Mean of each band for each AOI.

    '''
    plots = overlaps_flightline(datafile, shapefiles)
    
    with rasterio.open(datafile) as img_rstr:
        bands = img_rstr.descriptions    
    
        df = pd.DataFrame()
        
        for shp in plots.plot_no:
            # print('Processing Plot %s.' % (shp))
            window = img_rstr.window(*plots[plots['plot_no']==shp].bounds.iloc[0])
            shapes = ((geom,value) for geom, value in zip(plots[plots['plot_no']==shp].geometry, plots[plots['plot_no']==shp].plot_no))
            #get rows,cols of windowed area#
            rows,cols = img_rstr.read(5, window=from_bounds(*plots[plots['plot_no']==shp].bounds.iloc[0], transform=img_rstr.transform),boundless=True).shape
            result = rasterize(shapes=shapes,out_shape=(rows,cols),transform=img_rstr.window_transform(window))
            for index,band in enumerate(img_rstr.descriptions):
                array = img_rstr.read(index+1, window=from_bounds(*plots[plots['plot_no']==shp].bounds.iloc[0], transform=img_rstr.transform))
                ary = np.ma.masked_where(result!=shp,array).compressed()
                df.loc[bands[index],shp+'_mean'] = ary.mean()
                df.loc[bands[index],shp+'_std'] = ary.std()
                # print(band)
            print('Plot %s complete.' % (shp))
    return df


def data_extraction(inRaster, inPolygons, band):
    '''
    Statisticaly subsample band values from input raster. Subsampling defined by input polygons. 
    
    Polygons CRS will be transformed to match input raster CRS.

    Parameters
    ----------
    inRaster : rasterio Dataset
        Must be 3 bands (RGB) and georeferenced.
    inPolygons : GeoJSON 
        Polygons must have unique ID fields.
    samples : list
        List of statistical samples to be applied to data.
    band : str
        String of data source band information e.g. RGB.

    Returns
    -------
    pandas Dataframe
        Dataframe of statistical samples for each polygon AOI, indexed by uniqe ID.

    '''
    plots = overlaps_flightline(inRaster, inPolygons)
    # plots = plots.to_crs(get_raster_projection(inRaster))
    
    cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
    df=pd.DataFrame(columns=cols,index=range(0,(plots.id.size+1)))
    counter = 0
    
    outBands = {}
    for plot in plots.id:
        if band.upper() == 'RGB':
            r,g,b = rasterMasker(plots.set_index('id').loc[plot], inRaster)[0:3,...]
            outBands['Red'] = r.compressed()
            outBands['Green'] = g.compressed()
            outBands['Blue'] = b.compressed()
            del r,g,b
        
        if band.upper() == 'NIR':
            nir = rasterMasker(plots.set_index('id').loc[plot], inRaster, [1])
            outBands['NIR'] = nir.compressed()
            del nir
            
        if band.upper() == 'RGB' and band.upper() == 'NIR':
            r = rasterMasker(plots.set_index('id').loc[plot], inRaster, [1])
            
        if band.upper() == 'VNIR':
            vnir_mask, transform, window, bandage = rasterMasker(plots.set_index('id').loc[plot],RGB)
            for index, band in enumerate(bands):
                array = img_rstr.read(index+1, window=from_bounds(*plots[plots['plot_no']==shp].bounds.iloc[0], transform=img_rstr.transform))

        
        for key in outBands.keys():
            df.iloc[int(plot)-1].Plot=int(plot)
            df.iloc[int(plot)-1].Mean=np.mean(outBands[key])
            df.iloc[int(plot)-1].Median=np.median(outBands[key])
            df.iloc[int(plot)-1].StDv=np.std(outBands[key])
            df.iloc[int(plot)-1].Prcnt20=np.percentile(outBands[key],20)
            df.iloc[int(plot)-1].Prcnt80=np.percentile(outBands[key],80)   
        
        counter+=1
        percent_complete = (counter/plots.id.size)*100
        if percent_complete %1 ==0:
            print('%s%%' % (int(percent_complete)))
    return df

    
 

RGB = r'Y:\Morocco Field Experiment\MOROCCO INRA DRONE DATA\Vol 1\zone 1/inra_zone1_vol1_orthophoto.tif'

plots =(r'\\pepper\homestd\My Documents\Downloads\Zone_1_Vol_1_Plots\morroccoZone1.geojson')
   
 
#%% Dead Code
    # with rasterio.open(RGB) as tiff_rgb:
    #     cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
    #     df=pd.DataFrame(columns=cols,index=range(0,(plots.size+1)))
    #     plots = plots.to_crs(tiff_rgb.crs)
    #     for feature in plots.id:
    #         window = tiff_rgb.window(*plots.iloc[feature].geometry.bounds)
    #         rows,cols = tiff_rgb.read(1, window=from_bounds(*plots.iloc[feature].geometry.bounds, transform=tiff_rgb.transform),boundless=True).shape
    #         shapes = ((geom,value) for geom, value in zip(plots.iloc[feature].geometry, plots[plots['id']==feature].id))
    #         result = rasterize(shapes=shapes,out_shape=(rows,cols),transform=tiff_rgb.window_transform(window))
        
    #         r,g,b = tiff_rgb.read(([1,2,3]),window=from_bounds(*plots.iloc[feature].geometry.bounds, transform=tiff_rgb.transform))[0:3,...]       
    #         r=np.ma.masked_where(result!=feature,r).compressed()
    #         g=np.ma.masked_where(result!=feature,g).compressed()
    #         b=np.ma.masked_where(result!=feature,b).compressed()
    #         redcount=len(r)
            
    #         bands = {'Red':r,'Green':g,'Blue':b}
            
    #         for key in bands.keys():
    #             df.iloc[int(feature)-1].Plot=int(feature)
    #             df.loc[int(feature)-1].Mean=np.mean(bands[key])
    #             df.loc[int(feature)-1].Median=np.median(bands[key])
    #             df.loc[int(feature)-1].StDv=np.std(bands[key])
    #             df.loc[int(feature)-1].Prcnt20=np.percentile(bands[key],20)
    #             df.loc[int(feature)-1].Prcnt80=np.percentile(bands[key],80)
            
    #         counter+=1
    #         percent_complete = (counter/plots.id.size)*100
    #         if percent_complete %1 ==0:
    #             print('%s%%' % (int(percent_complete)))
            
                    # df.loc[int(name)-1].CanopyPercent_ExGR=(((mskd==1).sum())/redcount)*100
                    # print(((mskd==1).sum()),redcount)
                    # df.to_excel(outfile,sheet_name=key)
                    
        # def rgb_extraction(inRaster, inPolygons, samples):
        #     '''
        #     Statisticaly subsample band values from input raster. Subsampling defined by input polygons. 
            
        #     Polygons CRS will be transformed to match input raster CRS.

        #     Parameters
        #     ----------
        #     inRaster : rasterio Dataset
        #         Must be 3 bands (RGB) and georeferenced.
        #     inPolygons : GeoJSON 
        #         Polygons must have unique ID fields.
        #     samples : list
        #         List of statistical samples to be applied to data.

        #     Returns
        #     -------
        #     pandas Dataframe
        #         Dataframe of statistical samples for each polygon AOI, indexed by uniqe ID.

        #     '''
        #     plots = geopandas.read_file(inPolygons)
        #     plots = plots.to_crs(get_raster_projection(inRaster))
            
        #     cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
        #     df=pd.DataFrame(columns=cols,index=range(0,(plots.id.size+1)))
        #     counter = 0
            
        #     for plot in plots.id:
        #         r,g,b = rasterMasker(plots.set_index('id').loc[plot], inRaster, [1,2,3])[0:3,...]

        #         bands = {'Red':r.compressed(),'Green':g.compressed(),'Blue':b.compressed()}
                
        #         for key in bands.keys():
        #             df.iloc[int(plot)-1].Plot=int(plot)
        #             df.iloc[int(plot)-1].Mean=np.mean(bands[key])
        #             df.iloc[int(plot)-1].Median=np.median(bands[key])
        #             df.iloc[int(plot)-1].StDv=np.std(bands[key])
        #             df.iloc[int(plot)-1].Prcnt20=np.percentile(bands[key],20)
        #             df.iloc[int(plot)-1].Prcnt80=np.percentile(bands[key],80)   
                
        #         counter+=1
        #         percent_complete = (counter/plots.id.size)*100
        #         if percent_complete %1 ==0:
        #             print('%s%%' % (int(percent_complete)))
        #     return df