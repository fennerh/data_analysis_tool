# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 09:33:35 2021

@author: holmanf
"""
def rasterMasker(inPolygon, inRaster, band):
    '''
    Step 1) Window (subset) and rasterize GeoJSON polygon to same extent as inRaster.
    
    Step 2) Mask inRaster using rasterised polygon.

    Parameters
    ----------
    inPolygon : GeoJSON Polygon
        CRS needs to match inRaster.
    inRaster : rasterio Dataset
        Dataset to be masked.
    band : list of int
        List of band numbers to be masked from inPolygon.

    Returns
    -------
    Masked raster dataset.

    '''
    with rasterio.open(inRaster) as src:
        shp = inPolygon
        window = src.window(*shp.geometry.bounds)
        rows,cols = src.read(band[0], window=from_bounds(*shp.geometry.bounds, transform=src.transform),boundless=True).shape
        print(rows, cols)
        res = rasterize(shapes=([(shp.geometry,shp.name)]),out_shape=(rows,cols),transform=src.window_transform(window))
        res = np.repeat(res[np.newaxis,...],len(band),0)
        array = src.read((band), window=from_bounds(*shp.geometry.bounds, transform=src.transform))
        result = np.ma.masked_where(res!=shp.name,array)
    return result

def reproject_rasters(src_raster, repro_raster, inPolygon):
    
    with rasterio.open(src_raster) as src:
        with rasterio.open(repro_raster) as out:
            # wndw_src = src.read(1, window=from_bounds(*inPolygon.geometry.bounds, transform=src.transform))
            # wndw_out = out.read(1, window=from_bounds(*inPolygon.geometry.bounds, transform=out.transform))
            msk_src = rasterMasker(plot,RGB, [1])
            msk_out = rasterMasker(plot,NR, [1])
            dstny = np.empty([msk_out.shape[1],msk_out.shape[2]],dtype='float32')
            
            rasterio.warp.reproject(
                source=msk_src,
                destination=dstny,
                src_transform=src.window_transform(src.window(*inPolygon.geometry.bounds)),
                src_crs=src.crs,
                dst_transform=out.window_transform(out.window(*inPolygon.geometry.bounds)),
                dst_crs=out.crs,
                resampling=rasterio.warp.Resampling.nearest)
    dstny = np.ma.masked_where(np.ma.getmask(nir)[0],r)       
    return dstny, msk_out
        
plots = geopandas.read_file(r'E:\2017\plots.geojson')
RGB = r'E:\2017\1701_21_270717\RGB_ortho.tif'
NR = r'E:\2017\1701_21_270717\NIR_ortho.tif'
plots = plots.to_crs(get_raster_projection(RGB))

plot = plots.set_index('Id').loc[344]

dst_crs = get_raster_projection(RGB)


# def normalise_dsm(inDEM,inDSM,inPolygon):
#     '''
#     Normalise Digital Surface Model using Digital Elevation Model as 'bare ground' model.
    
#     If polygon provided, crop and normalise DSM by polygon.

#     Parameters
#     ----------
#     inDEM : rasterio Dataset
#         Digital Elevation Model dataset to be opened by rasterio.
#     inDSM : rasterio Dataset
#         Digital Surface Model dataset to be opened by rasterio.
#     inPolygon : TYPE
#         DESCRIPTION.

#     Returns
#     -------
#     Returns raster of normalised DSM. 
    
#     Output raster is projects to same CRS as DEM.

#     '''
#     dst_crs = get_raster_projection(inDSM)
#     with rasterio.open(inDEM,'r') as grnd:
#         ground, out_transform=mask(grnd,inPolygon,crop=True,nodata=0)
#         transform, width, height = rasterio.warp.calculate_default_transform(
#                grnd.crs, dst_crs, grnd.width, grnd.height, *grnd.bounds)
#         ground=ground[0,...]
#         dstny=np.empty([ground.shape[0],ground.shape[1]],dtype='float32')
             
#     with rasterio.open(inDSM,'r') as dem:
#         crop, crop_transform=mask(dem,inPolygon,crop=True,nodata=0)
#         transform, width, height = rasterio.warp.calculate_default_transform(
#                dem.crs, dst_crs, dem.width, dem.height, *dem.bounds)
#         crops=crop[0,...]
        
#         rasterio.warp.reproject(
#             source=crops,
#             destination=dstny,
#             src_transform=crop_transform,
#             src_crs=dem.crs,
#             dst_transform=out_transform,
#             dst_crs=dst_crs,
#             resampling=rasterio.warp.Resampling.nearest)
    
#     norm_dem=(dstny-ground)
#     return(norm_dem)