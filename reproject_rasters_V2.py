# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 09:08:47 2021

@author: holmanf
"""

def rasterMasker(inPolygon, inRaster, band):
    '''
    Step 1) Window (subset) and rasterize GeoJSON polygon to same extent as inRaster.
    
    Step 2) Mask inRaster using rasterised polygon.

    Parameters
    ----------
    inPolygon : GeoJSON Polygon
        CRS needs to match inRaster.
    inRaster : rasterio Dataset
        Dataset to be masked.
    band : list of int
        List of band numbers to be masked from inPolygon.

    Returns
    -------
    Masked raster dataset.

    '''
    if rasterio.features.is_valid_geom(inPolygon.geometry) == False:
       raise ValueError('Invalid geometry object, check GeoJSON.')
   
    
    with rasterio.open(inRaster) as src:
        shp = inPolygon
        window = src.window(*shp.iloc[0].bounds)
        rows,cols = src.read(band[0], window=from_bounds(*shp.iloc[0].bounds, transform=src.transform),boundless=True).shape
        res = rasterize(shapes=([(shp.geometry,shp.name)]),out_shape=(rows,cols),transform=src.window_transform(window))
        res = np.repeat(res[np.newaxis,...],len(band),0)
        array = src.read((band), window=from_bounds(*shp.iloc[0].bounds, transform=src.transform))
        result = [np.ma.masked_where(res!=shp.name,array),src.window_transform(window)]
    return result



def reproject_raster(src_raster, repro_raster, src_transform, repro_transform, crs):
    
    if len(repro_raster.shape) == 3:
        repro_raster = repro_raster[0]
    dstny = np.empty([repro_raster.shape[0],repro_raster.shape[1]],dtype='float32')        
    rasterio.warp.reproject(
        source=src_raster,
        destination=dstny,
        src_transform=src_transform,
        src_crs=crs,
        dst_transform=repro_transform,
        dst_crs=crs,
        resampling=rasterio.warp.Resampling.nearest)
    dstny = np.ma.masked_where(np.ma.getmask(repro_raster),dstny)       
    return dstny

plots = geopandas.read_file(r'E:\2017\plots.geojson')
RGB = r'E:\2017\1701_21_270717\RGB_ortho.tif'
NIR = r'E:\2017\1701_21_270717\NIR_ortho.tif'
plots = plots.to_crs(get_raster_projection(RGB))

plot = plots.set_index('id').loc[144]

cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
df=pd.DataFrame(columns=cols,index=range(0,(361)))

for plot in plots.id:
    r, r_transform = rasterMasker(plots.set_index('id').loc[plot], RGB, [1])[0:2]
    nir, nir_transform = rasterMasker(plots.set_index('id').loc[plot], NIR, [1])[0:2]
    
    nir_R = reproject_raster(nir, r, nir_transform, r_transform, get_raster_projection(RGB))
    ndvi = ((nir_R - r)/(nir_R + r)).compressed()
    
    df.iloc[int(plot)-1].Plot=int(plot)
    df.iloc[int(plot)-1].Mean=np.mean(ndvi)
    df.iloc[int(plot)-1].Median=np.median(ndvi)
    df.iloc[int(plot)-1].StDv=np.std(ndvi)
    df.iloc[int(plot)-1].Prcnt20=np.percentile(ndvi,20)
    df.iloc[int(plot)-1].Prcnt80=np.percentile(ndvi,80)   
        